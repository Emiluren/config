set -e

# yay
sudo pacman -S --needed git base-devel

if pacman -Qi yay > /dev/null ; then
    echo "Yay already installed"
else
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
    cd ..
    rm -rf yay/
fi

# i3
sudo pacman -S --needed i3-wm i3blocks ttf-font-awesome rofi \
     maim xclip `# screenshots` \
     alsa-utils `# for amixer` \
     sysstat `# for mpstat (cpu usage)` \
     wireless_tools `# for iwgetid (wifi name)` \
     acpi `# to show battery level` \
     xss-lock `# lock automatically on suspend` \
     inetutils `# for hostname`
yay -S --needed xcwd-git i3lock-color
git clone https://github.com/vivien/i3blocks-contrib/ ~/.config/i3/i3blocks-contrib

# .Xresources
yay -S --needed urxvt-tabbedex ttf-symbola-free
sudo pacman -S --needed xorg-fonts-misc xorg-xset xorg-mkfontscale

# Remember to run the following command in /usr/share/fonts to be able to load fonts in urxvt
# for dir in * ; do if [  -d  "$dir"  ]; then cd "$dir";xset +fp "$PWD" ;mkfontscale; mkfontdir;cd .. ;fi; done && xset fp rehash

# zsh
sudo pacman -S --needed zsh

# Audio
sudo pacman -S --needed pulseaudio pulseaudio-alsa pavucontrol

# Sway
yay -S --needed swaylock-effects-git foot
sudo pacman -S --needed sway grim slurp jq xorg-xsetroot
