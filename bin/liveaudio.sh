#!/bin/sh

MIC=alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback
HEADPHONES=alsa_output.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.analog-stereo

# Set up virtual streams
MOD1=$(pactl load-module module-null-sink sink_name=mic_and_game)
MOD2=$(pactl load-module module-null-sink sink_name=game)

# Connect things that will always be the same
MOD3=$(pactl load-module module-loopback source=$MIC sink=mic_and_game)
MOD4=$(pactl load-module module-loopback source=game.monitor sink=mic_and_game)
MOD5=$(pactl load-module module-loopback source=game.monitor sink=$HEADPHONES)

# Wait for user input
read -p "Press RET to quit..." ASDF

# Clean up
pactl unload-module $MOD1
pactl unload-module $MOD2
pactl unload-module $MOD3
pactl unload-module $MOD4
pactl unload-module $MOD5
