#!/bin/sh
case $(hostname) in
     emil-arch-desktop)
         xrandr --output DP-0 --primary --mode 1920x1200 --pos 0x360 --rotate normal \
                --output DP-4 --mode 1920x1200 --pos 1920x0 --rotate right
         echo 'Using work screen layout'
         ;;
     *)
         echo 'Unknown hostname'
         ;;
esac

~/.fehbg
