# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load completion
autoload -Uz compinit
compinit

# Load fzf (fuzzy finder) config
if [ -f /usr/share/fzf ]; then
    source /usr/share/fzf/completion.zsh
    #source /usr/share/fzf/key-bindings.zsh
fi

# Make completion case-insensitive
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# Set prompt to show current directory to the right
PROMPT='%# '
RPROMPT=$'%~'

# Set up command history
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Share history between zsh's
setopt SHARE_HISTORY

# Don't store duplicate commands in history or any that start with space
setopt HIST_IGNORE_ALL_DUPS HIST_IGNORE_SPACE

# Don't count / as part of a word
WORDCHARS=${WORDCHARS/\/}

# Hide user and host from window title
export DISABLE_AUTO_TITLE="true"

# Set window title for xterm to current directory
case $TERM in
    (*xterm* | rxvt* | foot)
        # Write some info to terminal title.
        # This is seen when the shell prompts for input.
        function precmd {
            print -Pn "\e]0;%(1j,%j job%(2j|s|); ,)%~\a"
        }
        # Write command and args to terminal title.
        # This is seen while the shell waits for a command to complete.
        function preexec {
            printf "\033]0;%s\a" "$1"
        }
    ;;
esac

# Make Ctrl-s search forward instead of suspending output
stty -ixon

alias sbcl="rlwrap sbcl"
alias ls="ls --color"
alias please=sudo

alias dh=dirs -v # List directory stack
DIRSTACKSIZE=8
setopt autopushd pushdminus pushdsilent pushdtohome

# Emit OSC 7 escape sequence for current directory
_urlencode() {
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:$i:1}"
        case $c in
            %) printf '%%%02X' "'$c" ;;
            *) printf "%s" "$c" ;;
        esac
    done
}
osc7_cwd() {
    printf '\e]7;file://%s%s\e\\' "$HOSTNAME" "$(_urlencode "$PWD")"
}
autoload -Uz add-zsh-hook
add-zsh-hook -Uz chpwd osc7_cwd

if [ -f ~/.zshrc_local.sh ]; then
    source ~/.zshrc_local.sh
fi

# virtualenvwrapper for python
export WORKON_HOME=~/.virtualenvs
source /usr/bin/virtualenvwrapper.sh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
